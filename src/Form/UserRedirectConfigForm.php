<?php

namespace Drupal\user_redirect\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Site\Settings;

/**
 * UserRedirectConfigForm Class Doc Comment.
 *
 * @category UserRedirectConfigForm
 * @package Form
 */
class UserRedirectConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_redirect_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildForm($form, $form_state);
    $user_redirect_config = $this->config('user_redirect.settings');

    $form['user_redirect_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Settings'),
      '#open' => TRUE,
    ];

    $form['user_redirect_settings']['login_redirect'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Login redirect'),
      '#maxlength' => 120,
      '#default_value' => $user_redirect_config->get('login_redirect'),
      '#description' => t('Insert route for login redirect'),
    ];

    $form['user_redirect_settings']['register_redirect'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Register redirect'),
      '#maxlength' => 120,
      '#default_value' => $user_redirect_config->get('register_redirect'),
      '#description' => t('Insert route for register redirect'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('user_redirect.settings');
    $config->set('login_redirect', $form_state->getValue('login_redirect'));
    $config->set('register_redirect', $form_state->getValue('register_redirect'));

    $config->save();
    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['user_redirect.settings'];
  }

}
